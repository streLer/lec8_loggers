package com.mycompany.l10;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingExample {

    private static final Logger logger = LoggerFactory.getLogger(LoggingExample.class);

    private int counter = 0;

    public static void main(String[] args) throws InterruptedException {
        var value = "John";

        logger.info("hello: {}", value);

        try {
            throw new RuntimeException("exception message");
        } catch (Exception ex) {
            logger.error("log exception:", ex);
        }

        //new LoggingExample().loop();
    }

    public void loop() throws InterruptedException {
        while (true) {
            logger.trace("trace level:{}", counter);
            logger.debug("debug level:{}", counter);
            logger.info("info level:{}", counter);
            logger.warn("warn level:{}", counter);
            logger.error("error level:{}", counter);
            counter++;
            System.out.println();
            Thread.sleep(3_000);
        }
    }
}
