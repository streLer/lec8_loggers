package com.mycompany.l10.listeners;

import lombok.SneakyThrows;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.nio.file.Files;

public class TestNgListener implements ITestListener {

    private static final Logger logger = LoggerFactory.getLogger(TestNgListener.class);

    @Override
    public void onTestStart(ITestResult result) {

    }

    @Override
    public void onTestSuccess(ITestResult result) {

    }

    @SneakyThrows
    @Override
    public void onTestFailure(ITestResult result) {
        File tmp = ((TakesScreenshot) result.getTestContext().getAttribute("driver"))
                .getScreenshotAs(OutputType.FILE);
        File screenShot = new File("screen-" + System.currentTimeMillis() + ".png");

        Files.copy(tmp.toPath(), screenShot.toPath());
        logger.info("Screenshot file: {}", screenShot.getAbsolutePath());
    }

    @Override
    public void onTestSkipped(ITestResult result) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {

    }

    @Override
    public void onFinish(ITestContext context) {

    }
}
