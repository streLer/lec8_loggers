package com.mycompany.l10.listeners;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyWebDriverListener extends AbstractWebDriverEventListener {

    private static final Logger logger = LoggerFactory.getLogger(MyWebDriverListener.class);

    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        logger.info("Selenium searches an element by {}", by);
    }

    @Override
    public void afterFindBy(By by, WebElement element, WebDriver driver) {
        logger.info("Selenium found an element by {}", by);
    }

    @Override
    public void beforeClickOn(WebElement element, WebDriver driver) {
        logger.info("Selenium clicks on an element");
    }

    @Override
    public void afterClickOn(WebElement element, WebDriver driver) {
        logger.info("Selenium clicked on an element");
    }

}
