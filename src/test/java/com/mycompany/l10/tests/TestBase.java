package com.mycompany.l10.tests;

import com.mycompany.l10.listeners.MyWebDriverListener;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class TestBase {

    EventFiringWebDriver driver;

    @BeforeMethod
    public void initData() {
        driver = new EventFiringWebDriver(new FirefoxDriver());
        driver.register(new MyWebDriverListener());

        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://intra.t-systems.ru/");
    }

    @AfterMethod
    public void stop() {
        if (driver != null) {
            driver.quit();
        }
    }

}