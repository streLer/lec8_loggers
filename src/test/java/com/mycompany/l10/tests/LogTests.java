package com.mycompany.l10.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.logging.Level;

/**
 * Firefox:
 * org.openqa.selenium.UnsupportedCommandException: GET /session/11db83bb-10d0-40cd-96b6-e20abde44688/log/types did not match a known command
 */
public class LogTests {

    private static final Logger logger = LoggerFactory.getLogger(LogTests.class);

    private WebDriver driver;

    @BeforeClass
    public void initData() {
        ChromeOptions chromeOptions = new ChromeOptions();
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.BROWSER, Level.ALL);
        logPrefs.enable(LogType.CLIENT, Level.ALL);
        logPrefs.enable(LogType.DRIVER, Level.ALL);
        logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
        chromeOptions.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);

        driver = new ChromeDriver(chromeOptions);
        driver.get("https://intra.t-systems.ru/");
    }

    @Test
    public void jsConsoleLogs() {
        driver.manage().logs().get(LogType.BROWSER).getAll()
                .forEach(logEntry -> logger.info("Log entry: {}", logEntry));
        String A = "test";
        String B = "tes2t";
        if (A == B) {
            logger.info("A equals B");
        }
        else
        {
            logger.info("A NOT equals B");
        }
    }

    @AfterMethod
    public void stop() {
        if (driver != null) {
            driver.quit();
        }
    }
}
