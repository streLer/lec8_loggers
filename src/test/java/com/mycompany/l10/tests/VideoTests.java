package com.mycompany.l10.tests;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class VideoTests extends TestBase {

    private final VideoRecorder video = new VideoRecorder();

    @BeforeMethod
    public void startRecording() {
        video.startRecording(driver);
    }

    @Test
    public void loginPositive() {
        driver.findElement(By.id("LOGINUSERNAME")).clear();
        driver.findElement(By.id("LOGINUSERNAME")).sendKeys("test");
        driver.findElement(By.id("LOGINPASSWORD")).clear();
        driver.findElement(By.id("LOGINPASSWORD")).sendKeys("testPW");
        driver.findElement(By.id("LOGINENTERBUTTON")).click();


        driver.findElement(By.id("tewtwtwt")).click();
    }

    @AfterMethod(alwaysRun = true)
    public void stopRecording() {
        video.stopRecording("Record done");
    }
}
