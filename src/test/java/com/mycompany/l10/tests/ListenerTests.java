package com.mycompany.l10.tests;


import com.mycompany.l10.listeners.TestNgListener;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

@Listeners(TestNgListener.class)
public class ListenerTests extends TestBase {

    @BeforeMethod
    public void setContext(ITestContext testContext) {
        testContext.setAttribute("driver", driver);
    }

    @Test
    public void loginNegative() {
        WebElement userName = driver.findElement(By.id("LOGINUSERNAME"));
        userName.clear();
        userName.sendKeys("TEST");

        WebElement password = driver.findElement(By.id("LOGINPASSWORD"));
        password.clear();
        password.sendKeys("NEGATIVE PW");

        driver.findElement(By.id("LOGINENTERBUTTON")).click();

        assertTrue(driver.findElement(By.className("validation-summary-errors"))
                        .getText().contains("Оши1бка входа"),
                "Expected error 'Ошибка входа' is not present");
    }

}
