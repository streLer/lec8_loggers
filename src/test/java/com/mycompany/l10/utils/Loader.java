package com.mycompany.l10.utils;

import java.io.InputStream;
import java.util.Properties;

class Loader {
    private static final String APP_PROPERTIES = "application.properties";

    static String loadProperty(String name) {
        Properties props = new Properties();
        InputStream is = Loader.class.getResourceAsStream(APP_PROPERTIES);

        try (is) {
            props.load(is);

            return props.getProperty(name);
        } catch (Throwable t) {
            throw throwAsUncheckedException(t);
        }
    }

    @SuppressWarnings("unchecked")
    private static <T extends Throwable> RuntimeException throwAsUncheckedException(Throwable t) throws T {
        throw (T) t;
    }
}
